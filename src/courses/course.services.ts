import { DeleteResult, UpdateResult } from 'typeorm';
import { AppDataSource } from '../configs/database/data-source';
import { HttpStatuses } from '../application/enums/http-status.enums';
import HttpException from '../application/exceptions/http-exceptions';
import { Course } from './entities/course.entity';
import { ICourse } from './types/course.interface';

export const courseRepository = AppDataSource.getRepository(Course);

export const getAllCourses = async (): Promise<Course[]> => {
  return await courseRepository.find({});
};

export const getCourseById = async (id: number): Promise<Course> => {
  const course = await courseRepository.findOne({ where: { id } });

  if (!course) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Course with this id not found',
    );
  }
  return course;
};

export const getCoursesByLectorId = async (id: number): Promise<Course[]> => {
  const lectorCourses = await courseRepository
    .createQueryBuilder('course')
    .select(['course.id as "courseId"', 'course.name as "courseName"'])
    .innerJoin('course.lectors', 'lector')
    .addSelect('lector.name as "lectorName"')
    .where('lector.id = :lectorId', { lectorId: id })
    .getRawMany();

  return lectorCourses;
};

export const createCourse = async (
  createCourseSchema: Omit<ICourse, 'id'>,
): Promise<Course> => {
  const course = await courseRepository.findOne({
    where: { name: createCourseSchema.name },
  });

  if (course) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Course with such name already exist',
    );
  }

  return courseRepository.save(createCourseSchema);
};

export const updateCourseById = async (
  id: number,
  updateCourseSchema: Partial<ICourse>,
): Promise<UpdateResult> => {
  const result = await courseRepository.update(id, updateCourseSchema);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Course with this id not found',
    );
  }

  return result;
};

export const deleteCourseById = async (id: number): Promise<Course> => {
  const course = await courseRepository.findOne({ where: { id } });

  if (!course) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Course with this id not found',
    );
  }

  course.lectors = [];

  const result = await courseRepository.remove(course);

  return result;
};
