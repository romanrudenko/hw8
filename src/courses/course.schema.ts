import Joi from 'joi';
import { ICourse } from './types/course.interface';

export const courseCreateSchema = Joi.object<Omit<ICourse, 'id'>>({
  name: Joi.string().min(2).required(),
  description: Joi.string().min(2).required(),
  hours: Joi.number().min(1).required(),
});

export const courseUpdateSchema = Joi.object<Partial<ICourse>>({
  name: Joi.string().min(2).optional(),
  description: Joi.string().min(2).optional(),
  hours: Joi.number().min(1).optional(),
});