import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { ICourse } from './course.interface';

/**
 * @swagger
 * components:
 *   schemas:
 *     CourseCreate:
 *       type: object
 *       required:
 *         - name
 *         - description
 *         - hourse
 *       properties:
 *         name:
 *           type: string
 *           description: The name of course
 *         description:
 *           type: string
 *           description: The description of course
 *         hours: 
 *           type: number
 *           description: The hours of course        
 *       example:
 *         name: Object-Oriented Programming
 *         description: Descrtiprion of this course
 *         hours: 10
 */

export interface ICourseCreateInterface extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<ICourse, 'id'>;
}
