import { ICourse } from "./course.interface";

/**
 * @swagger
 * components:
 *   schemas:
 *     Course:
 *       type: object
 *       required:
 *         - id
 *         - createdAt
 *         - updatedAt
 *         - name
 *         - description
 *         - hours
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the course
 *         createdAt:
 *           type: string
 *           format: date-time
 *           description: created at time
 *         updatedAt: 
 *           type: string
 *           format: date-time
 *           description: updated at time
 *         name:
 *           type: string
 *           description: The name of course
 *         description:
 *           type: string
 *           description: The description of course
 *         hours:
 *           type: number
 *           description: The hours of course
 *       example:
 *         id: 1
 *         createdAt: 2020-03-10T04:05:06.157Z
 *         updatedAt: 2020-03-10T04:05:06.157Z
 *         name: Object-Oriented Programming
 *         description: Descrtiprion of this course
 *         hours: 10
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     CourseByLectorId:
 *       type: object
 *       required:
 *         - coursId
 *         - courseName
 *         - lectorName
 *       properties:
 *         courseId:
 *           type: string
 *           description: The id of course
 *         courseName:
 *           type: string
 *           description: The name of course
 *         lectorName:
 *           type: number
 *           description: The name of lector
 *       example:
 *         courseId: 1
 *         courseName: Object-Oriented Programming
 *         lectorName: John Doe
 */

export interface ICourseGetResponse extends ICourse {
  createdAt: Date,
  updateAt: Date
}