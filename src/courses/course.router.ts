import { Router } from 'express';
import * as courseController from './course.controller';
import controllerWrapper from '../application/utils/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import idParamSchema from '../application/schemas/id-param.schema';
import { courseCreateSchema, courseUpdateSchema } from './course.schema';

const courseRouter = Router();

/**
 * @swagger
 * tags:
 *   name: Courses
 *   description: The course managing API
 * /courses:
 *   get:
 *     summary: The list of courses
 *     tags: [Courses]
 *     responses:
 *       200:
 *         description: All courses.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Course'
 *       500:
 *         description: Some server error
 *
 */

courseRouter.get('/', controllerWrapper(courseController.getAllCourses));

/**
 * @swagger
 * tags:
 *   name: Courses
 *   description: The courses managing API
 * /courses/{id}:
 *   get:
 *     summary: Get course by id
 *     tags: [Courses]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Course id
 *     responses:
 *       200:
 *         description: The course by id.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Course'
 *       404:
 *         description: The course was not found
 *       500:
 *         description: Some server error
 *
 */

courseRouter.get(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(courseController.getCourseById),
);

/**
 * @swagger
 * tags:
 *   name: Courses
 *   description: The courses managing API
 * /courses/lector/{id}:
 *   get:
 *     summary: Get courses by lector id
 *     tags: [Courses]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Lector id
 *     responses:
 *       200:
 *         description: The list of courses by lector id.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/CourseByLectorId'
 *       404:
 *         description: The lector was not found
 *       500:
 *         description: Some server error
 *
 */

courseRouter.get(
  '/lector/:id',
  validator.params(idParamSchema),
  controllerWrapper(courseController.getCoursesByLectorId),
);

/**
 * @swagger
 * tags:
 *   name: Courses
 *   description: The courses managing API
 * /courses:
 *   post:
 *     summary: Create course
 *     tags: [Courses]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CourseCreate' 
 *     responses:
 *       201:
 *         description: Return the course after creation.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Course'
 *       400:
 *         description: Course with such name already exist
 *       500:
 *         description: Some server error
 *
 */

courseRouter.post(
  '/',
  validator.body(courseCreateSchema),
  controllerWrapper(courseController.createCourse),
);

/**
 * @swagger
 * tags:
 *   name: Courses
 *   description: The courses managing API
 * /courses/{id}:
 *   patch:
 *     summary: Update course by id
 *     tags: [Courses]
 *     requestBody:
 *       required: false
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CourseUpdate' 
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Course id
 *     responses:
 *       204:
 *         description: Course was updated
 *       404:
 *         description: Course was not found
 *       500:
 *         description: Some server error
 *
 */

courseRouter.patch(
  '/:id',
  validator.params(idParamSchema),
  validator.body(courseUpdateSchema),
  controllerWrapper(courseController.updateCourseById),
);

/**
 * @swagger
 * tags:
 *   name: Courses
 *   description: The courses managing API
 * /courses/{id}:
 *   delete:
 *     summary: Delete course by id
 *     tags: [Courses]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Course id
 *     responses:
 *       204:
 *         description: Course was deleted
 *       404:
 *         description: Course was not found
 *       500:
 *         description: Some server error
 *
 */

courseRouter.delete(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(courseController.deleteCourseById),
);

export default courseRouter;
