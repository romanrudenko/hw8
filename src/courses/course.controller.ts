import { Request, Response } from 'express';
import * as courseServices from './course.services';
import { ValidatedRequest } from 'express-joi-validation';
import { ICourseCreateInterface } from './types/course-create-request.interface';
import { ICourseUpdateInterface } from './types/course-update-request.interface';

export const getAllCourses = async (req: Request, res: Response) => {
  const courses = await courseServices.getAllCourses();
  res.status(200).json(courses);
};

export const getCourseById = async (
  req: Request<{ id: number }>,
  res: Response,
) => {
  const { id } = req.params;
  const course = await courseServices.getCourseById(id);
  res.status(200).json(course);
};

export const getCoursesByLectorId = async (
  req: Request<{ id: number }>,
  res: Response,
) => {
  const { id } = req.params;
  const course = await courseServices.getCoursesByLectorId(id);
  res.status(200).json(course);
};

export const createCourse = async (
  req: ValidatedRequest<ICourseCreateInterface>,
  res: Response,
) => {
  const course = await courseServices.createCourse(req.body);
  res.status(201).json(course);
};

export const updateCourseById = async (
  req: ValidatedRequest<ICourseUpdateInterface>,
  res: Response,
) => {
  const { id } = req.params;
  const course = await courseServices.updateCourseById(id, req.body);
  res.status(200).json(course);
};

export const deleteCourseById = async (
  req: Request<{ id: number }>,
  res: Response,
) => {
  const { id } = req.params;
  const course = await courseServices.deleteCourseById(id);
  res.status(200).json(course);
};
