import Joi from 'joi';

const idParamSchema = Joi.object<{ id: string }>({
  id: Joi.number().required(),
});

export default idParamSchema;