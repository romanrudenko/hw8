import express from 'express';
import cors from 'cors';
import studentRouter from '../students/students.router';
import exceptionFilter from './middlewares/exceptions.filter';
import bodyParser from 'body-parser';
import path from 'path';
import swaggerJsdoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';
import { AppDataSource } from '../configs/database/data-source';
import groupsRouter from '../groups/groups.router';
import courseRouter from '../courses/course.router';
import lectorRouter from '../lectors/lector.router';
import markRouter from '../marks/marks.router';

const app = express();

app.use(cors());
app.use(bodyParser.json());

const options = {
  definition: {
    openapi: '3.1.0',
    info: {
      title: 'University API',
      version: '0.1.0',
      description: 'University API',
      license: {
        name: 'MIT',
        url: 'https://spdx.org/licenses/MIT.html',
      },
    },
    servers: [
      {
        url: 'http://localhost:3000/api/v1',
      },
    ],
  },
  apis: ['../**/*.router.ts', '../**/*.interface.ts'],
};

const specs = swaggerJsdoc(options);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs));

AppDataSource.initialize()
  .then(() => {
    console.log('TypeORM connected to database');
  })
  .catch((error) => {
    console.log('Error:', error);
  });

const staticFilesPath = path.join(__dirname, '../', 'public');

app.use('/api/v1/public', express.static(staticFilesPath));
app.use('/api/v1/students', studentRouter);
app.use('/api/v1/groups', groupsRouter);
app.use('/api/v1/courses', courseRouter);
app.use('/api/v1/lectors', lectorRouter);
app.use('/api/v1/marks', markRouter);

app.use(exceptionFilter);

export default app;
