import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateStudentTable1688064724533 implements MigrationInterface {
    name = 'CreateStudentTable1688064724533'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "students" ADD "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "students" ADD "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "students" DROP COLUMN "updated_at"`);
        await queryRunner.query(`ALTER TABLE "students" DROP COLUMN "created_at"`);
    }

}
