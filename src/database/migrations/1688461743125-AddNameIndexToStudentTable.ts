import { MigrationInterface, QueryRunner } from "typeorm";

export class AddNameIndexToStudentTable1688461743125 implements MigrationInterface {
    name = 'AddNameIndexToStudentTable1688461743125'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE INDEX "name1-idx" ON "students" ("name") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."name1-idx"`);
    }

}
