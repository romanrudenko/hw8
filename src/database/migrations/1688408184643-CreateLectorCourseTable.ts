import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateLectorCourseTable1688408184643 implements MigrationInterface {
    name = 'CreateLectorCourseTable1688408184643'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "lector_course" ("lectorsId" integer NOT NULL, "coursesId" integer NOT NULL, CONSTRAINT "PK_0c226038a877fcddda42045e6e6" PRIMARY KEY ("lectorsId", "coursesId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_0df9a2b79106263bbe926980ed" ON "lector_course" ("lectorsId") `);
        await queryRunner.query(`CREATE INDEX "IDX_fed6d4e40a7626912ab1c6e80c" ON "lector_course" ("coursesId") `);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "FK_0df9a2b79106263bbe926980ed8" FOREIGN KEY ("lectorsId") REFERENCES "lectors"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "FK_fed6d4e40a7626912ab1c6e80cf" FOREIGN KEY ("coursesId") REFERENCES "courses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "FK_fed6d4e40a7626912ab1c6e80cf"`);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "FK_0df9a2b79106263bbe926980ed8"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_fed6d4e40a7626912ab1c6e80c"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_0df9a2b79106263bbe926980ed"`);
        await queryRunner.query(`DROP TABLE "lector_course"`);
    }

}
