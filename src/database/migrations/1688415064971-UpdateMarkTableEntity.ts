import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdateMarkTableEntity1688415064971 implements MigrationInterface {
    name = 'UpdateMarkTableEntity1688415064971'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "marks" ADD "student_id" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "marks" ADD "lector_id" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "marks" ADD "course_id" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "marks" ADD CONSTRAINT "FK_5226e1592e6291dbe7a07640346" FOREIGN KEY ("student_id") REFERENCES "students"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "marks" ADD CONSTRAINT "FK_69b5af3347a46bb74ccc3c6f65c" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "marks" ADD CONSTRAINT "FK_3e39a10631f1c639777a2b99cb8" FOREIGN KEY ("course_id") REFERENCES "courses"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "marks" DROP CONSTRAINT "FK_3e39a10631f1c639777a2b99cb8"`);
        await queryRunner.query(`ALTER TABLE "marks" DROP CONSTRAINT "FK_69b5af3347a46bb74ccc3c6f65c"`);
        await queryRunner.query(`ALTER TABLE "marks" DROP CONSTRAINT "FK_5226e1592e6291dbe7a07640346"`);
        await queryRunner.query(`ALTER TABLE "marks" DROP COLUMN "course_id"`);
        await queryRunner.query(`ALTER TABLE "marks" DROP COLUMN "lector_id"`);
        await queryRunner.query(`ALTER TABLE "marks" DROP COLUMN "student_id"`);
    }

}
