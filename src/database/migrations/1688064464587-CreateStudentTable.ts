import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateStudentTable1688064464587 implements MigrationInterface {
    name = 'CreateStudentTable1688064464587'

    

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "students" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "surname" character varying NOT NULL, "email" character varying NOT NULL, "age" integer NOT NULL, "image_path" character varying, CONSTRAINT "PK_659d1483316afb28afd3a90787e" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "students"`);
    }

}