import { DeleteResult, UpdateResult } from 'typeorm';
import { AppDataSource } from '../configs/database/data-source';
import { HttpStatuses } from '../application/enums/http-status.enums';
import HttpException from '../application/exceptions/http-exceptions';
import { Mark } from './entities/mark.entity';
import { IMark } from './types/marks.interface';

const markRepository = AppDataSource.getRepository(Mark);

export const getAllMarks = async (): Promise<Mark[]> => {
  return await markRepository.find({});
};

export const getMarkById = async (id: number): Promise<Mark> => {
  const course = await markRepository.findOne({ where: { id } });

  if (!course) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Mark with this id not found',
    );
  }
  return course;
};

export const getStudentMarksById = async (id: number): Promise<Mark[]> => {
  const studentMarks = await markRepository
    .createQueryBuilder('mark')
    .select('mark.mark as mark')
    .leftJoin('mark.courses', 'course')
    .addSelect('course.name as "courseName"')
    .where('mark.studentId = :id', { id })
    .getRawMany();

  if (!studentMarks) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  return studentMarks;
};

export const getCourseMarksById = async (id: number): Promise<Mark[]> => {
  const studentMarks = await markRepository
    .createQueryBuilder('mark')
    .select('mark.mark as mark')
    .leftJoin('mark.courses', 'course')
    .leftJoin('mark.students', 'student')
    .leftJoin('mark.lectors', 'lector')
    .addSelect([
      'course.name as "courseName"',
      'student.name as "studentName"',
      'lector.name as "lectoName"',
    ])
    .where('mark.courseId = :id', { id })
    .getRawMany();

  if (!studentMarks) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Course not found');
  }

  return studentMarks;
};

export const createMark = async (
  createMarkSchema: Omit<IMark, 'id'>,
): Promise<Mark> => {
  return markRepository.save(createMarkSchema);
};

export const updateMarkById = async (
  id: number,
  updateMarkSchema: Partial<IMark>,
): Promise<UpdateResult> => {
  const result = await markRepository.update(id, updateMarkSchema);

  if (!result.affected) {
    ('');
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Mark with this id not found',
    );
  }

  return result;
};

export const deleteMarkById = async (id: number): Promise<DeleteResult> => {
  const result = await markRepository.delete(id);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Mark with this id not found',
    );
  }

  return result;
};
