import Joi from 'joi';
import { IMark } from './types/marks.interface';

export const markCreateSchema = Joi.object<Omit<IMark, 'id'>>({
  mark: Joi.number().min(1).required(),
  lectorId: Joi.number().min(1).required(),
  studentId: Joi.number().min(1).required(),
  courseId: Joi.number().min(1).required(),
});

export const markUpdateSchema = Joi.object<Partial<IMark>>({
  mark: Joi.number().min(1).optional(),
  lectorId: Joi.number().min(1).optional(),
  studentId: Joi.number().min(1).optional(),
  courseId: Joi.number().min(1).optional(),
});
