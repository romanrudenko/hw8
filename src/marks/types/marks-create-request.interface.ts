import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IMark } from './marks.interface';

/**
 * @swagger
 * components:
 *   schemas:
 *     MarkCreate:
 *       type: object
 *       required:
 *         - mark
 *         - lectorId
 *         - studentId
 *         - courseId
 *       properties:
 *         mark:
 *           type: number
 *           description: Mark
 *         lectorId:
 *           type: string
 *           description: The id of lector
 *         studentId:
 *           type: string
 *           description: The id of student
 *         courseId:
 *           type: string
 *           description: The id of course  
 *       
 *       example:
 *         mark: 5
 *         lectorId: 2
 *         studentId: 3
 *         courseId: 4
 */

export interface IMarkCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<IMark, 'id'>;
}
