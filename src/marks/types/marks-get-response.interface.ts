import { IMark } from './marks.interface';

/**
 * @swagger
 * components:
 *   schemas:
 *     Mark:
 *       type: object
 *       required:
 *         - id
 *         - createdAt
 *         - updatedAt
 *         - mark
 *         - lectorId
 *         - studentId
 *         - courseId
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the lector
 *         createdAt:
 *           type: string
 *           format: date-time
 *           description: created at time
 *         updatedAt:
 *           type: string
 *           format: date-time
 *           description: updated at time
 *         mark:
 *           type: number
 *           description: Mark
 *         lectorId:
 *           type: string
 *           description: The id of lector
 *         studentId:
 *           type: string
 *           description: The id of student
 *         courseId:
 *           type: string
 *           description: The id of course
 *
 *       example:
 *         id: 1
 *         createdAt: 2020-03-10T04:05:06.157Z
 *         updatedAt: 2020-03-10T04:05:06.157Z
 *         mark: 5
 *         lectorId: 2
 *         studentId: 3
 *         courseId: 4
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     CourseMarks:
 *       type: object
 *       properties:
 *         mark:
 *           type: number
 *           description: The mark
 *         courseName:
 *           type: string
 *           description: The name of course
 *         studentName:
 *           type: string
 *           description: The name of student
 *         lectorName:
 *           type: string
 *           description: The name of lector
 *       example:
 *         mark: 5
 *         courseName: Art
 *         studentName: John Doe
 *         lectorName: Jack Luck
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     StudentMarks:
 *       type: object
 *       properties:
 *         mark:
 *           type: number
 *           description: The mark
 *         courseName:
 *           type: string
 *           description: The name of course
 *       example:
 *         mark: 5
 *         courseName: Art
 */

export interface IMarkGetResponse extends IMark {
  createdAt: Date;
  updatedAt: Date;
}
