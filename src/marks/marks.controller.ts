import { Request, Response, query } from 'express';
import * as markServices from './marks.services';
import { ValidatedRequest } from 'express-joi-validation';
import { IMarkCreateRequest } from './types/marks-create-request.interface';
import { IMarkUpdateRequest } from './types/marks-update-request.interface';

export const getAllMarks = async (req: Request, res: Response) => {
  const marks = await markServices.getAllMarks();
  res.status(200).json(marks);
};

export const getStudentMarksById = async (
  req: Request<{ id: number }>,
  res: Response,
) => {
  const { id } = req.params;
  const studentMarks = await markServices.getStudentMarksById(id);
  res.status(200).json(studentMarks);
};

export const getCourseMarksById = async (
  req: Request<{ id: number }>,
  res: Response,
) => {
  const { id } = req.params;
  const courseMarks = await markServices.getCourseMarksById(id);
  res.status(200).json(courseMarks);
};

export const createMark = async (
  req: ValidatedRequest<IMarkCreateRequest>,
  res: Response,
) => {
  const mark = await markServices.createMark(req.body);
  res.status(201).json(mark);
};

export const updateMarkById = async (
  req: ValidatedRequest<IMarkUpdateRequest>,
  res: Response,
) => {
  const { id } = req.params;
  await markServices.updateMarkById(id, req.body);
  res.status(204).end();
};

export const deleteMarkById = async (
  req: Request<{ id: number }>,
  res: Response,
) => {
  const { id } = req.params;
  await markServices.deleteMarkById(id);
  res.status(204).end();
};
