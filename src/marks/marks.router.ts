import { Router } from 'express';
import * as markController from './marks.controller';
import controllerWrapper from '../application/utils/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import idParamSchema from '../application/schemas/id-param.schema';
import { markCreateSchema, markUpdateSchema } from './marks.schema';

const markRouter = Router();

/**
 * @swagger
 * tags:
 *   name: Marks
 *   description: The marks managing API
 * /marks:
 *   get:
 *     summary: The list of marks
 *     tags: [Marks]
 *     responses:
 *       200:
 *         description: All marks.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Mark'
 *       500:
 *         description: Some server error
 *
 */

markRouter.get('/', controllerWrapper(markController.getAllMarks));

/**
 * @swagger
 * tags:
 *   name: Marks
 *   description: The marks managing API
 * /marks/student/{id}:
 *   get:
 *     summary: Get student marks by id
 *     tags: [Marks]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Student id
 *     responses:
 *       200:
 *         description: The student marks by id.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/StudentMarks'
 *       404:
 *         description: The student was not found
 *       500:
 *         description: Some server error
 *
 */

markRouter.get(
  '/student/:id',
  validator.params(idParamSchema),
  controllerWrapper(markController.getStudentMarksById),
);

/**
 * @swagger
 * tags:
 *   name: Marks
 *   description: The marks managing API
 * /marks/course/{id}:
 *   get:
 *     summary: Get course marks by id
 *     tags: [Marks]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Course id
 *     responses:
 *       200:
 *         description: The course marks by id.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/CourseMarks'
 *       404:
 *         description: The course was not found
 *       500:
 *         description: Some server error
 *
 */

markRouter.get(
  '/course/:id',
  validator.params(idParamSchema),
  controllerWrapper(markController.getCourseMarksById),
);

/**
 * @swagger
 * tags:
 *   name: Marks
 *   description: The marks managing API
 * /marks:
 *   post:
 *     summary: Create mark
 *     tags: [Marks]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/MarkCreate' 
 *     responses:
 *       201:
 *         description: Return the mark after creation.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Mark'
 *       500:
 *         description: Some server error
 *
 */

markRouter.post(
  '/',
  validator.body(markCreateSchema),
  controllerWrapper(markController.createMark),
);

/**
 * @swagger
 * tags:
 *   name: Marks
 *   description: The marks managing API
 * /marks/{id}:
 *   patch:
 *     summary: Update mark by id
 *     tags: [Marks]
 *     requestBody:
 *       required: false
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/MarkUpdate' 
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Mark id
 *     responses:
 *       204:
 *         description: Mark was updated.
 *       404:
 *         description: Mark was not found
 *       500:
 *         description: Some server error
 *
 */

markRouter.patch(
  '/:id',
  validator.params(idParamSchema),
  validator.body(markUpdateSchema),
  controllerWrapper(markController.updateMarkById),
);

/**
 * @swagger
 * tags:
 *   name: Marks
 *   description: The marks managing API
 * /marks/{id}:
 *   delete:
 *     summary: Delete mark by id
 *     tags: [Marks]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Mark id
 *     responses:
 *       204:
 *         description: Mark was deleted
 *       404:
 *         description: Mark was not found
 *       500:
 *         description: Some server error
 *
 */

markRouter.delete(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(markController.deleteMarkById),
);

export default markRouter;
