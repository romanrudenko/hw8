import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Student } from '../../students/entites/student.entity';
import { Lector } from '../../lectors/entities/lector.entity';
import { Course } from '../../courses/entities/course.entity';

@Entity({ name: 'marks' })
export class Mark extends CoreEntity {
  @Column({
    type: 'integer',
    nullable: false,
  })
  mark: number;

  @ManyToOne(() => Student, (student) => student.mark, {
    nullable: false,
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'student_id' })
  students: Student;

  @Column({
    name: 'student_id',
    type: 'integer',
    nullable: false,
  })
  studentId: number;

  @ManyToOne(() => Lector, (lector) => lector.mark, {
    nullable: false,
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'lector_id' })
  lectors: Student;

  @Column({
    name: 'lector_id',
    type: 'integer',
    nullable: false,
  })
  lectorId: number;

  @ManyToOne(() => Course, (course) => course.mark, {
    nullable: false,
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'course_id' })
  courses: Student;

  @Column({
    name: 'course_id',
    type: 'integer',
    nullable: false,
  })
  courseId: number;
}
