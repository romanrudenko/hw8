import Joi from 'joi';
import { IGroup } from './types/groups.interface';

export const groupCreateSchema = Joi.object<Omit<IGroup, 'id'>>({
  name: Joi.string().min(2).required(),
});

export const groupUpdateSchema = Joi.object<Partial<IGroup>>({
  name: Joi.string().min(2).optional(),
});
