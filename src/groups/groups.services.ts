import { DeleteResult, UpdateResult } from 'typeorm';
import { AppDataSource } from '../configs/database/data-source';
import { HttpStatuses } from '../application/enums/http-status.enums';
import HttpException from '../application/exceptions/http-exceptions';
import { Group } from './entities/group.entity';
import { IGroup } from './types/groups.interface';

export const groupRepository = AppDataSource.getRepository(Group);

export const getAllGroups = async (): Promise<Group[]> => {
  const groups = await groupRepository
    .createQueryBuilder('group')
    .leftJoinAndSelect('group.students', 'student')
    .getMany();

  return groups;
};

export const getGroupById = async (id: number): Promise<Group> => {
  const group = await groupRepository
    .createQueryBuilder('group')
    .leftJoinAndSelect('group.students', 'student')
    .where('group.id = :id', { id })
    .getOne();

  if (!group) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Group with this id not found',
    );
  }

  return group;
};

export const createGroup = async (
  createGroupSchema: Omit<IGroup, 'id'>,
): Promise<Group> => {
  const group = await groupRepository.findOne({
    where: { name: createGroupSchema.name },
  });

  if (group) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Group with such name already exist',
    );
  }

  return groupRepository.save(createGroupSchema);
};

export const updateGroupById = async (
  id: number,
  updateGroupSchema: Partial<IGroup>,
): Promise<UpdateResult> => {
  const result = await groupRepository.update(id, updateGroupSchema);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Group with this id not found',
    );
  }

  return result;
};

export const deleteGroupById = async (id: number): Promise<DeleteResult> => {
  const result = await groupRepository.delete(id);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Group with this id not found',
    );
  }

  return result;
};
