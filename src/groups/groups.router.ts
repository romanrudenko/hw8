import { Router } from 'express';
import * as groupController from './groups.controller';
import controllerWrapper from '../application/utils/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import idParamSchema from '../application/schemas/id-param.schema';
import { groupCreateSchema, groupUpdateSchema } from './groups.schema';

const groupsRouter = Router();

/**
 * @swagger
 * tags:
 *   name: Groups
 *   description: The groups managing API
 * /groups:
 *   get:
 *     summary: The list of groups
 *     tags: [Groups]
 *     responses:
 *       200:
 *         description: All groups.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Group'
 *       500:
 *         description: Some server error
 *
 */

groupsRouter.get('/', controllerWrapper(groupController.getAllGroups));

/**
 * @swagger
 * tags:
 *   name: Groups
 *   description: The groups managing API
 * /groups/{id}:
 *   get:
 *     summary: Get group by id
 *     tags: [Groups]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Group id
 *     responses:
 *       200:
 *         description: The group by id.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Group'
 *       404:
 *         description: The group was not found
 *       500:
 *         description: Some server error
 *
 */

groupsRouter.get(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(groupController.getGroupById),
);

/**
 * @swagger
 * tags:
 *   name: Groups
 *   description: The groups managing API
 * /groups:
 *   post:
 *     summary: Create group
 *     tags: [Groups]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/GroupCreate' 
 *     responses:
 *       201:
 *         description: Return the group after creation.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Group'
 *       400:
 *         description: Group with such name already exist
 *       500:
 *         description: Some server error
 *
 */

groupsRouter.post(
  '/',
  validator.body(groupCreateSchema),
  controllerWrapper(groupController.createGroup),
);

/**
 * @swagger
 * tags:
 *   name: Groups
 *   description: The groups managing API
 * /groups/{id}:
 *   patch:
 *     summary: Update group by id
 *     tags: [Groups]
 *     requestBody:
 *       required: false
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/GroupUpdate' 
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Group id
 *     responses:
 *       204:
 *         description: Group was updated
 *       404:
 *         description: Group was not found
 *       500:
 *         description: Some server error
 *
 */

groupsRouter.patch(
  '/:id',
  validator.params(idParamSchema),
  validator.body(groupUpdateSchema),
  controllerWrapper(groupController.updateGroud),
);

/**
 * @swagger
 * tags:
 *   name: Groups
 *   description: The groups managing API
 * /groups/{id}:
 *   delete:
 *     summary: Delete group by id
 *     tags: [Groups]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Group id
 *     responses:
 *       204:
 *         description: Group was deleted
 *       404:
 *         description: Group was not found
 *       500:
 *         description: Some server error
 *
 */

groupsRouter.delete(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(groupController.deleteGroup),
);

export default groupsRouter;
