import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IGroup } from './groups.interface';

/**
 * @swagger
 * components:
 *   schemas:
 *     GroupUpdate:
 *       type: object
 *       properties:
 *         name:
 *           type: string
 *           description: The name of group          
 *       example:
 *         name: CS-15
 */

export interface IGroupUpdateInterface extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<IGroup>;
}
