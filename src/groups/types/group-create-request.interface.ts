import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IGroup } from './groups.interface';

/**
 * @swagger
 * components:
 *   schemas:
 *     GroupCreate:
 *       type: object
 *       required:
 *         - name
 *       properties:
 *         name:
 *           type: string
 *           description: The name of group         
 *       example:
 *         name: CS-20
 */

export interface IGroupCreateInterface extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<IGroup, 'id'>;
}
