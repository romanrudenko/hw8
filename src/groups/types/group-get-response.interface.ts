import { IStudentGetResponse } from "../../students/types/student-get-response.interface";
import { IGroup } from "./groups.interface";

/**
 * @swagger
 * components:
 *   schemas:
 *     Group:
 *       type: object
 *       required:
 *         - id
 *         - createdAt
 *         - updatedAt
 *         - name
 *         - students
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the group
 *         createdAt:
 *           type: string
 *           format: date-time
 *           description: created at time
 *         updatedAt: 
 *           type: string
 *           format: date-time
 *           description: updated at time
 *         name:
 *           type: string
 *           description: The name of group
 *         students:
 *           type: array
 *           description: The list of students, wich belongs to group
 *       example:
 *         id: 1
 *         createdAt: 2020-03-10T04:05:06.157Z
 *         updatedAt: 2020-03-10T04:05:06.157Z
 *         name: CS-20
 *         students: []
 */

export interface IGroupGetResponse extends IGroup{
  createdAt: Date,
  updatedAt: Date,
  students: Array<IStudentGetResponse>
}