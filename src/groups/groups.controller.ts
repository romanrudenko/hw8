import { Request, Response } from 'express';
import * as groupServices from './groups.services';
import { ValidatedRequest } from 'express-joi-validation';
import { IGroupCreateInterface } from './types/group-create-request.interface';
import { IGroupUpdateInterface } from './types/group-update-request.interface';

export const getAllGroups = async (req: Request, res: Response) => {
  const groups = await groupServices.getAllGroups();
  res.status(200).json(groups);
};

export const getGroupById = async (req: Request<{ id: number }>, res: Response) => {
  const { id } = req.params;
  const group = await groupServices.getGroupById(id);
  res.status(200).json(group);
};

export const createGroup = async (
  req: ValidatedRequest<IGroupCreateInterface>,
  res: Response,
) => {
  const group = await groupServices.createGroup(req.body);
  res.status(201).json(group);
};

export const updateGroud = async (
  req: ValidatedRequest<IGroupUpdateInterface>,
  res: Response,
) => {
  const { id } = req.params;
  await groupServices.updateGroupById(id, req.body);
  res.status(204).end();
};

export const deleteGroup = async (
  req: Request<{id: number}>,
  res: Response
) => {
  const { id } = req.params;
  await groupServices.deleteGroupById(id);
  res.status(204).end();
}
