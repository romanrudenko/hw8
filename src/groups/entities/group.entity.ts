import { Column, Entity, OneToMany } from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Student } from '../../students/entites/student.entity';

@Entity({ name: 'groups' })
export class Group extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: true,
  })
  name: string;

  @OneToMany(() => Student, (student) => student.group)
  students: Student[];
}
