import { DeleteResult, UpdateResult } from 'typeorm';
import { AppDataSource } from '../configs/database/data-source';
import { HttpStatuses } from '../application/enums/http-status.enums';
import HttpException from '../application/exceptions/http-exceptions';
import { ILector } from './types/lector.interface';
import { Lector } from './entities/lector.entity';
import { courseRepository } from '../courses/course.services';

export const lectorRepository = AppDataSource.getRepository(Lector);

export const getAllLectors = async (): Promise<Lector[]> => {
  return await lectorRepository.find({});
};

export const getLectorById = async (id: number): Promise<Lector> => {
  const lector = await lectorRepository.findOne({
    where: { id },
    relations: {
      courses: true,
    },
  });

  if (!lector) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Lector with this id not found',
    );
  }
  return lector;
};

export const createLector = async (
  createLectorSchema: Omit<ILector, 'id'>,
): Promise<Lector> => {
  const lector = await lectorRepository.findOne({
    where: { email: createLectorSchema.email },
  });

  if (lector) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Lector with such email already exist',
    );
  }

  return lectorRepository.save(createLectorSchema);
};

export const addLectorToCourse = async (
  lectorId: number,
  courseId: number,
): Promise<Lector> => {
  const lector = await lectorRepository.findOne({
    where: { id: lectorId },
    relations: ['courses'],
  });
  const course = await courseRepository.findOne({ where: { id: courseId } });

  if (!course || !lector) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Course or lector not found',
    );
  }

  lector.courses.push(course);

  return await lectorRepository.save(lector);
};

export const updateLectorById = async (
  id: number,
  updateLectorSchema: Partial<ILector>,
): Promise<UpdateResult> => {
  const result = await lectorRepository.update(id, updateLectorSchema);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Lector with this id not found',
    );
  }

  return result;
};

export const deleteLectorById = async (id: number): Promise<Lector> => {
  const lector = await lectorRepository.findOne({
    where: { id },
    relations: ['courses'],
  });

  if (!lector) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Lector with this id not found',
    );
  }

  lector.courses = [];

  const result = await lectorRepository.remove(lector);

  return result;
};
