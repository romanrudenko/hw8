import Joi from 'joi';
import { ILector } from './types/lector.interface';
import { ILectoToCourse } from './types/lector-to-corse.interface';

export const lectorCreateSchema = Joi.object<Omit<ILector, 'id'>>({
  name: Joi.string().min(2).required(),
  email: Joi.string().email().required(),
  password: Joi.string().min(5).required(),
});

export const lectorUpdateSchema = Joi.object<Partial<ILector>>({
  name: Joi.string().min(2).optional(),
  email: Joi.string().email().optional(),
  password: Joi.string().min(5).optional(),
});

export const lectorToCourseSchema = Joi.object<Partial<ILectoToCourse>>({
  courseId: Joi.number().min(1).required(),
});
