
/**
 * @swagger
 * components:
 *   schemas:
 *     LectorToCourse:
 *       type: object
 *       required:
 *         - courseId
 *       properties:
 *         courseId:
 *           type: string
 *           description: The id of course
 *       example:
 *         courseId: 1
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     LectorToCourseResponse:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the lector
 *         createdAt:
 *           type: date
 *           description: created at time
 *         updatedAt: 
 *           type: date
 *           description: updated at time
 *         name:
 *           type: string
 *           description: The name of lector
 *         email:
 *           type: string
 *           format: emial
 *           description: The email of lector
 *         password:
 *           type: string
 *           format: password
 *           description: The password of lector
 *         courses:
 *           type: array
 *           descripion: The list of courses
 *       example:
 *         id: 1
 *         createdAt: 2020-03-10T04:05:06.157Z
 *         updatedAt: 2020-03-10T04:05:06.157Z
 *         name: Jack Luck
 *         email: jackluck@gmail.com
 *         password: jackluck22
 *         courses: []
 */
export interface ILectoToCourse {
  lectorId: number;
  courseId: number;
}
