import { ILector } from "./lector.interface";

/**
 * @swagger
 * components:
 *   schemas:
 *     Lector:
 *       type: object
 *       required:
 *         - id
 *         - createdAt
 *         - updatedAt
 *         - name
 *         - email
 *         - password
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the lector
 *         createdAt:
 *           type: string
 *           format: date-time
 *           description: created at time
 *         updatedAt: 
 *           type: string
 *           format: date-time
 *           description: updated at time
 *         name:
 *           type: string
 *           description: The name of lector
 *         email:
 *           type: string
 *           format: emain
 *           description: The email of lector
 *         password:
 *           type: string
 *           format: password
 *           description: The password of lector
 *          
 *       example:
 *         id: 1
 *         createdAt: 2020-03-10T04:05:06.157Z
 *         updatedAt: 2020-03-10T04:05:06.157Z
 *         name: Jack Luck
 *         email: jackluck@gmail.com
 *         password: jackluck22
 */

export interface ILectorGetResponse extends ILector {
  createdAt: Date,
  updatedAt: Date
}