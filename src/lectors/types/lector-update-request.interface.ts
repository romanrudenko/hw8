import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { ILector } from './lector.interface';

/**
 * @swagger
 * components:
 *   schemas:
 *     LectorUpdate:
 *       type: object
 *       properties:
 *         name:
 *           type: string
 *           description: The name of lector
 *         email:
 *           type: string
 *           format: email
 *           description: The email of lector
 *         password:
 *           type: string
 *           format: password
 *           description: The password of lector
 *       example:
 *         name: Jack Luck
 *         email: jackluck@gmail.com
 *         password: jackluck22
 */

export interface ILectorUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<ILector>;
}
