import { Router } from 'express';
import * as lectorContoller from './lector.controller';
import controllerWrapper from '../application/utils/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import idParamSchema from '../application/schemas/id-param.schema';
import { lectorCreateSchema, lectorToCourseSchema, lectorUpdateSchema } from './lector.schema';

const lectorRouter = Router();

/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The lectors managing API
 * /lectors:
 *   get:
 *     summary: The list of lectors
 *     tags: [Lectors]
 *     responses:
 *       200:
 *         description: All lectors.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Lector'
 *       500:
 *         description: Some server error
 *
 */

lectorRouter.get('/', controllerWrapper(lectorContoller.getAllLectors));

/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The lectors managing API
 * /lectors/{id}:
 *   get:
 *     summary: Get lector by id
 *     tags: [Lectors]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Lector id
 *     responses:
 *       200:
 *         description: The lector by id.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/LectorToCourseResponse'
 *       404:
 *         description: The lector was not found
 *       500:
 *         description: Some server error
 *
 */

lectorRouter.get(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(lectorContoller.getLectorById),
);

/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The lectors managing API
 * /lectors:
 *   post:
 *     summary: Create lector
 *     tags: [Lectors]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/LectorCreate' 
 *     responses:
 *       201:
 *         description: Return the lector after creation.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Lector'
 *       400:
 *         description: Lector with such email already exist
 *       500:
 *         description: Some server error
 *
 */

lectorRouter.post(
  '/',
  validator.body(lectorCreateSchema),
  controllerWrapper(lectorContoller.createLector),
);

/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The lectors managing API
 * /lectors/{id}/course:
 *   post:
 *     summary: Add lector to course
 *     tags: [Lectors]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Lector id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/LectorToCourse' 
 *     responses:
 *       201:
 *         description: Return the lector with courses.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/LectorToCourseResponse'
 *       404:
 *         description: Lector or course was not found
 *       500:
 *         description: Some server error
 *
 */

lectorRouter.post(
  '/:id/course',
  validator.params(idParamSchema),
  validator.body(lectorToCourseSchema),
  controllerWrapper(lectorContoller.addLectorToCourse),
);

/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The lectors managing API
 * /lectors/{id}:
 *   patch:
 *     summary: Update lector by id
 *     tags: [Lectors]
 *     requestBody:
 *       required: false
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/LectorUpdate' 
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Lector id
 *     responses:
 *       204:
 *         description: Lector was updated.
 *       404:
 *         description: Lector was not found
 *       500:
 *         description: Some server error
 *
 */

lectorRouter.patch(
  '/:id',
  validator.params(idParamSchema),
  validator.body(lectorUpdateSchema),
  controllerWrapper(lectorContoller.updateLectorById),
);

/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The lectors managing API
 * /lectors/{id}:
 *   delete:
 *     summary: Delete lector by id
 *     tags: [Lectors]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Lector id
 *     responses:
 *       204:
 *         description: Lector was deleted.
 *       404:
 *         description: Lector was not found
 *       500:
 *         description: Some server error
 *
 */

lectorRouter.delete(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(lectorContoller.deleteLectorById),
);

export default lectorRouter;
