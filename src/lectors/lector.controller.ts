import { Request, Response } from 'express';
import * as lectorServices from './lectors.services';
import { ValidatedRequest } from 'express-joi-validation';
import { ILectorCreateRequest } from './types/lector-create-request.interface';
import { ILectorUpdateRequest } from './types/lector-update-request.interface';

export const getAllLectors = async (req: Request, res: Response) => {
  const lectors = await lectorServices.getAllLectors();
  res.status(200).json(lectors);
};

export const getLectorById = async (
  req: Request<{ id: number }>,
  res: Response,
) => {
  const { id } = req.params;
  const lectors = await lectorServices.getLectorById(id);
  res.status(200).json(lectors);
};

export const createLector = async (
  req: ValidatedRequest<ILectorCreateRequest>,
  res: Response,
) => {
  const lector = await lectorServices.createLector(req.body);
  res.status(201).json(lector);
};

export const addLectorToCourse = async (
  req: Request<{id: number, courseId: number}>,
  res: Response 
) => {
  const { id } = req.params;
  const { courseId } = req.body;
  const result = await lectorServices.addLectorToCourse(id, courseId);
  res.status(200).json(result)
}

export const updateLectorById = async (
  req: ValidatedRequest<ILectorUpdateRequest>,
  res: Response,
) => {
  const { id } = req.params;
  await lectorServices.updateLectorById(id, req.body);
  res.status(204).end();
};

export const deleteLectorById = async (
  req: Request<{ id: number }>,
  res: Response,
) => {
  const { id } = req.params;
  await lectorServices.deleteLectorById(id);
  res.status(204).end();
};
