import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IStudent } from './students.interface';

/**
 * @swagger
 * components:
 *   schemas:
 *     StudentUpdate:
 *       type: object
 *       properties:
 *         name:
 *           type: string
 *           description: The name of student
 *         surname:
 *           type: string
 *           description: The surname of student
 *         email:
 *           type: string
 *           format: email
 *           description: The email of student
 *         age:
 *           type: number
 *           description: The age of student
 *       example:
 *         name: John
 *         surname: Doe
 *         email: johndoe@gmail.com
 *         age: 23
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     AddStudentGroup:
 *       type: object
 *       properties:
 *         groupId:
 *           type: number
 *           description: Id of the group
 *       example:
 *         groupId: 2
 */

export interface IStudentUpdateInterface extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<IStudent>;
}
