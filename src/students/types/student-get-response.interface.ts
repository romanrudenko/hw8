import { IStudent } from './students.interface';

/**
 * @swagger
 * components:
 *   schemas:
 *     Student:
 *       type: object
 *       required:
 *         - id
 *         - createdAt
 *         - updatedAt
 *         - name
 *         - surname
 *         - email
 *         - age
 *         - imagePath
 *         - groupName
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the student
 *         createdAt:
 *           type: string
 *           format: date-time
 *           description: created at time
 *         updatedAt: 
 *           type: string
 *           format: date-time
 *           description: updated at time
 *         name:
 *           type: string
 *           description: The name of student
 *         surname:
 *           type: string
 *           description: The surname of student
 *         email:
 *           type: string
 *           format: email
 *           description: The email of student
 *         age:
 *           type: number
 *           description: The age of student
 *         imagePath:
 *           type: string
 *           description: The path of student image
 *         groupName:
 *           type: string
 *           description: The name of student group
 *          
 *       example:
 *         id: 1
 *         createdAt: 2020-03-10T04:05:06.157Z
 *         updatedAt: 2020-03-10T04:05:06.157Z
 *         name: John
 *         surname: Doe
 *         email: johndoe@gmail.com
 *         age: 23
 *         imagePath: 10031233121-image.jpg
 *         groupName: "CS"
 */

export interface IStudentGetResponse extends IStudent {
  createdAt: Date;
  updatedAt: Date;
}
