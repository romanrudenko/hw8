import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IStudent } from './students.interface';

/**
 * @swagger
 * components:
 *   schemas:
 *     StudentCreate:
 *       type: object
 *       required:
 *         - name
 *         - surname
 *         - email
 *         - age
 *       properties:
 *         name:
 *           type: string
 *           description: The name of student
 *         surname:
 *           type: string
 *           description: The surname of student
 *         email:
 *           type: string
 *           format: email
 *           description: The email of student
 *         age:
 *           type: number
 *           description: The age of student *          
 *       example:
 *         name: John
 *         surname: Doe
 *         email: johndoe@gmail.com
 *         age: 23
 */

export interface IStudentCreateInterface extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<IStudent, 'id'>;
}
