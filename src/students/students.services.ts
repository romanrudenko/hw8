import ObjectID from 'bson-objectid';
import { HttpStatuses } from '../application/enums/http-status.enums';
import HttpException from '../application/exceptions/http-exceptions';
import path from 'path';
import * as fs from 'node:fs/promises';
import { AppDataSource } from '../configs/database/data-source';
import { Student } from './entites/student.entity';
import { DeleteResult, UpdateResult } from 'typeorm';
import { IStudent } from './types/students.interface';
import { groupRepository } from '../groups/groups.services';

const studentRepository = AppDataSource.getRepository(Student);

export const getAllStudents = async (name?: string): Promise<Student[]> => {
  let studentQuery = studentRepository
    .createQueryBuilder('student')
    .select([
      'student.id as id',
      'student.name as name',
      'student.surname as surname',
      'student.email as email',
      'student.age as age',
      'student.imagePath as imagePath',
      'student.createdAt as createdAt',
      'student.updatedAt as updateAt',
    ])
    .leftJoin('student.group', 'group')
    .addSelect('group.name as "groupName"');

  if (name !== undefined) {
    studentQuery = studentQuery.where('student.name = :name', { name });
  }

  const student = studentQuery.getRawMany();

  return student;
};

export const getStudentById = async (id: number): Promise<Student> => {
  const student = await studentRepository
    .createQueryBuilder('student')
    .select([
      'student.id as id',
      'student.name as name',
      'student.surname as surname',
      'student.email as email',
      'student.age as age',
      'student.imagePath as imagePath',
      'student.createdAt as createdAt',
      'student.updatedAt as updateAt',
    ])
    .leftJoin('student.group', 'group')
    .addSelect('group.name as "groupName"')
    .where('student.id = :id', { id })
    .getRawOne();

  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  return student;
};

export const createStudent = async (
  createStudentSchema: Omit<IStudent, 'id'>,
): Promise<Student> => {
  const student = await studentRepository.findOne({
    where: {
      email: createStudentSchema.email,
    },
  });

  if (student) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Student with this email already exist',
    );
  }

  return studentRepository.save(createStudentSchema);
};

export const updateStudentById = async (
  id: number,
  updateStudentSchema: Partial<IStudent>,
): Promise<UpdateResult> => {
  const result = await studentRepository.update(id, updateStudentSchema);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Student with this id not found',
    );
  }

  return result;
};

export const addImage = async (id: number, filePath?: string) => {
  if (!filePath) {
    throw new HttpException(HttpStatuses.BAD_REQUEST, 'File is not provided');
  }

  try {
    const imageId = ObjectID().toHexString();
    const imageExtention = path.extname(filePath);
    const imageName = imageId + imageExtention;

    const studentsDirectoryName = 'students';
    const studentsDirectoryPath = path.join(
      __dirname,
      '../../',
      'public',
      studentsDirectoryName,
    );

    const newImagePath = path.join(studentsDirectoryPath, imageName);
    const imagePath = `${studentsDirectoryName}/${imageName}`;

    await fs.rename(filePath, newImagePath);

    const updatedStudent = updateStudentById(id, { imagePath });

    return updatedStudent;
  } catch (error) {
    await fs.unlink(filePath);
    throw error;
  }
};

export const addGroupToStudent = async (
  studentId: number,
  groupId: number,
): Promise<UpdateResult> => {
  const group = await groupRepository.findOne({ where: { id: groupId } });

  if (!group) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Group with this id not found',
    );
  }

  const result = await studentRepository.update(studentId, { groupId });

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Student with this id not found',
    );
  }

  return result;
};

export const deleteStudentById = async (id: number): Promise<DeleteResult> => {
  const result = await studentRepository.delete(id);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Student with this id not found',
    );
  }

  return result;
};
