import { Request, Response } from 'express';
import * as studentServices from './students.services';
import { ValidatedRequest } from 'express-joi-validation';
import { IStudentCreateInterface } from './types/student-create-request.interface';
import { IStudentUpdateInterface } from './types/student-update-request.interface';

export const getAllStudents = async (req: Request, res: Response) => {
  const name = req.query.name as string;
  const students = await studentServices.getAllStudents(name);
  res.status(200).json(students);
};

export const getStudentById = async (
  req: Request<{ id: number }>,
  res: Response,
) => {
  const { id } = req.params;
  const student = await studentServices.getStudentById(id);
  res.status(200).json(student);
};

export const createStudent = async (
  req: ValidatedRequest<IStudentCreateInterface>,
  res: Response,
) => {
  const student = await studentServices.createStudent(req.body);
  res.status(201).json(student);
};

export const updateStudentById = async (
  req: ValidatedRequest<IStudentUpdateInterface>,
  res: Response,
) => {
  const { id } = req.params;
  await studentServices.updateStudentById(id, req.body);
  res.status(204).end();
};

export const addImage = (
  req: Request<{ id: number; file: Express.Multer.File }>,
  res: Response,
) => {
  const { id } = req.params;
  const { path } = req.file ?? {};
  const student = studentServices.addImage(id, path);
  res.status(200).json(student);
};

export const addGroupToStudent = async (
  req: Request<{ id: number; groupId: number }>,
  res: Response,
) => {
  const { id } = req.params;
  const { groupId } = req.body;
  await studentServices.addGroupToStudent(id, groupId);
  res.status(204).end();
};

export const deleteStudentById = async (
  req: Request<{ id: number }>,
  res: Response,
) => {
  const { id } = req.params;
  await studentServices.deleteStudentById(id);
  res.status(204).end();
};
