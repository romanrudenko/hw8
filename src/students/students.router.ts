import { Router } from 'express';
import * as studentController from './students.controller';
import controllerWrapper from '../application/utils/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import idParamSchema from '../application/schemas/id-param.schema';
import { studentCreateSchema, studentUpdateSchema } from './student.schema';
// import uploadMiddleware from '../application/middlewares/upload.middleware';

const studentRouter = Router();

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students:
 *   get:
 *     summary: The list of students
 *     tags: [Students]
 *     parameters:
 *       - in: query
 *         name: name
 *         schema:
 *           type: string
 *         description: Student name
 *     responses:
 *       200:
 *         description: All students.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Student'
 *       500:
 *         description: Some server error
 *
 */

studentRouter.get('/', controllerWrapper(studentController.getAllStudents));

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students/{id}:
 *   get:
 *     summary: Get student by id
 *     tags: [Students]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Student id
 *     responses:
 *       200:
 *         description: The student by id.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Student'
 *       404:
 *         description: The student was not found
 *       500:
 *         description: Some server error
 *
 */

studentRouter.get(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(studentController.getStudentById),
);

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students:
 *   post:
 *     summary: Create student
 *     tags: [Students]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/StudentCreate' 
 *     responses:
 *       201:
 *         description: Return the student after creation.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Student'
 *       400:
 *         description: Student with such email already exist
 *       500:
 *         description: Some server error
 *
 */

studentRouter.post(
  '/',
  validator.body(studentCreateSchema),
  controllerWrapper(studentController.createStudent),
);

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students/{id}:
 *   patch:
 *     summary: Update student by id
 *     tags: [Students]
 *     requestBody:
 *       required: false
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/StudentUpdate' 
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Student id
 *     responses:
 *       204:
 *         description: Student was updated
 *       404:
 *         description: Student was not found
 *       500:
 *         description: Some server error
 *
 */

studentRouter.patch(
  '/:id',
  validator.params(idParamSchema),
  validator.body(studentUpdateSchema),
  controllerWrapper(studentController.updateStudentById),
);

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students/{id}/group:
 *   patch:
 *     summary: Add group to student by id
 *     tags: [Students]
 *     requestBody:
 *       required: false
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/AddStudentGroup' 
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Student id
 *     responses:
 *       204:
 *         description: Group was added to student
 *       404:
 *         description: Student was not found
 *       500:
 *         description: Some server error
 *
 */

studentRouter.patch(
  '/:id/group',
  validator.params(idParamSchema),
  controllerWrapper(studentController.addGroupToStudent),
);

// studentRouter.patch(
//   '/:id/image',
//   validator.params(idParamSchema),
//   uploadMiddleware.single('file'),
//   controllerWrapper(studentController.addImage),
// );

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students/{id}:
 *   delete:
 *     summary: Delete student by id
 *     tags: [Students]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Student id
 *     responses:
 *       204:
 *         description: Student was deleted.
 *       404:
 *         description: Student was not found
 *       500:
 *         description: Some server error
 *
 */

studentRouter.delete(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(studentController.deleteStudentById),
);

export default studentRouter;
