import Joi from 'joi';
import { IStudent } from './types/students.interface';

export const studentCreateSchema = Joi.object<Omit<IStudent, 'id'>>({
  name: Joi.string().min(2).required(),
  surname: Joi.string().min(2).required(),
  email: Joi.string().email().required(),
  age: Joi.number().integer().min(16).required(),
  groupId: Joi.string().hex().length(24).optional(),
});

export const studentUpdateSchema = Joi.object<Partial<IStudent>>({
  name: Joi.string().min(2).optional(),
  surname: Joi.string().min(2).optional(),
  email: Joi.string().email().optional(),
  age: Joi.number().integer().min(16).optional(),
  groupId: Joi.string().hex().length(24).optional(),
});
